---
title: "PÉTITION : FACE AUX BLOCAGES ET DÉGRADATIONS, LES UNIVERSITÉS ET L’ETAT DOIVENT DÉPOSER PLAINTE ET SANCTIONNER LES CASSEURS !"
date: 2022-06-20T14:41:19+02:00
author: "l'Uni"
tags: ["pétition","Université"]
type: "post"
cover:
    image: img/uni.png
    alt: 'This is a post image'
    caption: ' This is the caption'
image: img/manif.png
---

Depuis les résultats du 1er tour de la présidentielle, les militants d’extrême-gauche n’ont de cesse de chercher de quelconques prétextes pour mettre en route leur pseudo lutte sociale et bloquent nos établissements. En réalité, les étudiants en sont les premières victimes. Leurs universités sont saccagées, ils ne peuvent plus entrer dans leur amphi, suivre leur cours, passer leurs examens (Sorbonne et Sciences Po notamment). On assiste à une véritable tyrannie d’une poignée de militants «antifas» sur les étudiants qui ne demandent qu’à étudier et réussir.

Face à ces actions illégales et scandaleuses, les universités et l’Etat ne font rien. C’est pourquoi, l’UNI lance une pétition pour la liberté d’étudier et pour demander des sanctions fermes à l’encontre des bloqueurs.

Obligeons l’Etat et les universités à réagir et exigeons qu’ils portent ENFIN plainte contre les casseurs et qu’ils mettent en place des mesures disciplinaires à l’égard des étudiants et lycéens concernés !
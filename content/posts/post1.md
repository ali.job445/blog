---
title: "L’accès à l’enseignement supérieur"
date: 2022-06-18T14:41:19+02:00
author: "l'Unef"
tags: ["Unef","Inscription","Université"]
type: "post"
cover:
    image: img/unef.png
    alt: 'This is a post image'
    caption: ' This is the caption'
---

L’accès à l’enseignement supérieur

Depuis 2018, Parcoursup est la plateforme qui remplace Admission Post-Bac pour accéder à l’université et à d’autres formations. La plateforme ouvre en janvier pour consulter les formations existantes. Le calendrier de la procédure change chaque année et est consultable sur le site de Parcoursup.

Chaque candidat·e doit remplir son dossier et choisir entre 1 et 10 vœux de formation pour recevoir des propositions d’admission entre les mois de mai et juillet durant la phase principale. Une phase complémentaire est également ouverte avant la fin de la phase principale et court jusqu’en septembre, pour ceux et celles qui n’auraient pas eu de réponse favorable dans la filière souhaitée.

Attention, depuis 2018**,** les universités ont le droit de refuser des candidat·e·s pour les formations de licence. L’UNEF s’oppose toujours à cette sélection qui est parfois effectuée automatiquement par un algorithme. A ce jour l’UNEF**,** demande la fin de cette sélection et la publication des algorithmes utilisés.

L’accès à l’enseignement supérieur peut également se faire par d’autres moyens :

– La demande d’inscription préalable (DAP) pour les étudiant·e·s étranger·ères souhaitant s’inscrire dans une première année à l’Université.

Plus d’info

– La procédure e-candidat au sein de chaque université**,** pour des candidatures à partir de la deuxième année après le Bac. Cette procédure commence courant mars et chaque université développe sa plateforme e-candidat en ligne.

Avoir une inscription dans le supérieur pour la première fois ou dans le cadre de poursuite d’études peut être parfois un parcours du combattant avec toutes ses formes de sélections et le manque de place dans nos établissements**,** faute d’investissement.

Afin, d’aider et d’accompagner les jeunes à l’accès à l’enseignement supérieur l’UNEF**,** aux côtés de l’UNL, la FCPE, le SAF et PLUS**,** a mis en place le dispositif SOS Inscription qui chaque année accompagne des milliers de jeunes sans inscription.



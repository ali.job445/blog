---
title: "Victoire pour la population étudiante : annulation de la certification en langue suite au recours au Conseil d’Etat réalisé par la FNAEL et 14 associations"
date: 2022-06-19T14:41:19+02:00
author: "la Fage"
tags: ["Fage","Certification","Université"]
type: "post"
cover:
    image: img/fage.png
    alt: 'This is a post image'
    caption: ' This is the caption'
---

Le 3 avril 2020, un décret et un arrêté rendant obligatoire, pour les étudiants et étudiantes inscrites en licence, licence professionnelle - BUT et BTS, le passage d’une certification en langue anglaise conditionnant la délivrance du diplôme, paraissent au Journal Officiel.

Dès leur parution soudaine et infondée, la FNAEL et la FAGE se sont fermement opposées à cette certification obligatoire aberrante. La certification induisait un risque de précarisation de la population étudiante puisque le financement de cette mesure n’apparaissait pas dans les textes : les étudiantes et étudiants pouvaient donc un jour être amenéEs à financer leur propre certification, ouvrant ainsi une porte aux organismes privés dans nos établissements d’enseignement supérieur publics. Pour preuve, nombre d’universités ont d’ores et déjà choisi des certificateurs privés.

D’autre part, elle devait être réalisée uniquement en anglais, et était donc contraire à nos valeurs de promotion du plurilinguisme. De plus, cette certification n’avait aucun réel impact sur l’apprentissage des langues des étudiantes et étudiants puisque seul le passage de la certification, et non l’obtention d’un niveau, était obligatoire.

Le 4 septembre 2020, la FNAEL et 14 associations nationales ont déposé un recours au Conseil d’Etat, afin d’abroger le décret et l’arrêté.

La décision du Conseil d’Etat a été rendue le 7 juin 2022 et nous a donné raison : le décret et l’arrêté rendant obligatoire le passage de la certification en langue anglaise sont annulés et réputés n’avoir jamais existé !

En effet, cette certification en langue anglaise allait à l’encontre du code de l’éducation disposant dans le deuxième alinéa de son article L613-1 que « les diplômes nationaux […] ne peuvent être délivrés qu’au vu des résultats du contrôle des connaissances et des aptitudes appréciés par les établissements accrédités ».

Suite à cette décision, la FNAEL et la FAGE interpellent le gouvernement et réclament :

un meilleur d’accès à l’apprentissage de différentes langues, dans une démarche de promotion du plurilinguisme, tant des langues internationales que de France
la mise en place des cours adaptés aux différents niveaux des étudiants et étudiantes.
plus de budget alloué au recrutement de professeurs de langues étrangères pour pallier le manque d’effectif
le remboursement par l’Etat des frais engagés pour le passage de cette certification, tant par les acteurs et actrices de l’enseignement supérieur que par les étudiantes et étudiants
Symbolisant une réelle victoire étudiante, la FNAEL et la FAGE saluent cette décision faisant suite à une mesure ne présentant aucun intérêt pour la population étudiante. Néanmoins, de réelles mesures garantissant un apprentissage des langues efficace sont attendues.